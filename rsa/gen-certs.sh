#!/bin/bash

# Bash shell script for generating self-signed certs. Run this in a folder, as it
# generates a few files. Large portions of this script were taken from the
# following artcile:
# 
# http://usrportage.de/archives/919-Batch-generating-SSL-certificates.html
# 
# Additional alterations by: Brad Landers
# Date: 2012-01-27
# usage: ./gen_cert.sh example.com

# Script accepts a single argument, the fqdn for the cert
CN_NAME="$1"
path=$CN_NAME

mkdir -p ./$path
rm -rf ./$path/*

# Strip the password so we don't have to type it every time we restart Apache
openssl rsa -in ./$path/$DOMAIN.key.org -out ./$path/$DOMAIN.key -passin env:PASSPHRASE

# Generate the cert (good for 10 years)
openssl x509 -req -days 3650 -in ./$path/$DOMAIN.csr -signkey ./$path/$DOMAIN.key -out ./$path/$DOMAIN.crt


# Certificate Authority
echo Generate CA key:
openssl genrsa -passout pass:1111 -des3 -out ./$path/ca.key 4096

echo Generate CA certificate:
openssl req -passin pass:1111 -new -x509 -days 365 -key ./$path/ca.key -out ./$path/ca.crt -subj "/CN=${CN_NAME}"

# Server side key
echo Generate server key:
openssl genrsa -passout pass:1111 -des3 -out ./$path/server.key 4096

echo Generate server signing request:
openssl req -passin pass:1111 -new -key ./$path/server.key -out ./$path/server.csr -subj "/CN=${CN_NAME}"

echo Self-signed server certificate:
openssl x509 -req -passin pass:1111 -days 365 -in ./$path/server.csr -CA ./$path/ca.crt -CAkey ./$path/ca.key -set_serial 01 -out ./$path/server.crt

echo Remove passphrase from server key:
openssl rsa -passin pass:1111 -in ./$path/server.key -out ./$path/server.key

# Client side key
echo Generate client key
openssl genrsa -passout pass:1111 -des3 -out ./$path/client.key 4096

echo Generate client signing request:
openssl req -passin pass:1111 -new -key ./$path/client.key -out ./$path/client.csr -subj "/CN=${CN_NAME}"

echo Self-signed client certificate:
openssl x509 -passin pass:1111 -req -days 365 -in ./$path/client.csr -CA ./$path/ca.crt -CAkey ./$path/ca.key -set_serial 01 -out ./$path/client.crt

echo Remove passphrase from client key:
openssl rsa -passin pass:1111 -in ./$path/client.key -out ./$path/client.key

openssl pkcs8 -topk8 -nocrypt -in ./$path/client.key -out ./$path/client.pem

openssl pkcs8 -topk8 -nocrypt -in ./$path/server.key -out ./$path/server.pem
