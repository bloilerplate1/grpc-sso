package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"v-sso/account"
	"v-sso/datasource"
	"v-sso/utilities/errors_handler"

	"github.com/dgrijalva/jwt-go"
)

// your imports here

// will initialize a handler starting from data sources
// which inject into repository layer
// which inject into service layer
// which inject into handler layer
type Injector struct {
}

func inject(ds *datasource.DataSources) (*account.AccountResolversConfig, *Interceptor, error) {
	log.Println("Injecting data sources")
	// handlerTimeout := os.Getenv("HANDLER_TIMEOUT")
	/*
	 * repository layer
	 */
	accountRepository := account.NewAccountRepository(&account.AccountRepositoryCong{
		MongoClient: ds.MongoDB,
	})

	privKeyFile := os.Getenv("PRIV_KEY_FILE")
	priv, err := ioutil.ReadFile(privKeyFile)

	if err != nil {
		return nil, nil, errors_handler.NewInternal(fmt.Sprintf("could not read private key pem file: %s", err))
	}

	privKey, err := jwt.ParseRSAPrivateKeyFromPEM(priv)

	if err != nil {
		return nil, nil, errors_handler.NewInternal(fmt.Sprintf("could not parse private key: %s", err))
	}

	pubKeyFile := os.Getenv("PUB_KEY_FILE")
	pub, err := ioutil.ReadFile(pubKeyFile)

	if err != nil {
		return nil, nil, errors_handler.NewInternal(fmt.Sprintf("could not read public key pem file: %s", err))
	}

	pubKey, err := jwt.ParseRSAPublicKeyFromPEM(pub)

	if err != nil {
		return nil, nil, errors_handler.NewInternal(fmt.Sprintf("could not parse public key: %s", err))
	}

	// load refresh token secret from env variable
	refreshSecret := os.Getenv("REFRESH_SECRET")
	// accessTokenExp := os.Getenv("ID_TOKEN_EXP")
	// accessTokenExp := "9"
	refreshTokenExp := os.Getenv("REFRESH_TOKEN_EXP")

	// accessExp, err := strconv.ParseInt(accessTokenExp, 0, 64)

	if err != nil {
		return nil, nil, errors_handler.NewInternal(fmt.Sprintf("could not parse ID_TOKEN_EXP as int: %s", err))
	}

	refreshExp, err := strconv.ParseInt(refreshTokenExp, 0, 64)

	if err != nil {
		return nil, nil, errors_handler.NewInternal(fmt.Sprintf("could not parse REFRESH_TOKEN_EXP as int: %s", err))
	}

	tokenFactory := account.NewTokenFactory(&account.TokenFactoryConfig{
		PrivKey:               privKey,
		PubKey:                pubKey,
		RefreshSecret:         refreshSecret,
		IDExpirationSecs:      2,
		RefreshExpirationSecs: refreshExp,
		MongoClient:           ds.MongoDB,
	})

	accountService := account.NewAccountService(&account.AccountServiceConfig{
		AccountRepository: accountRepository,
		TokenFactory:      tokenFactory,
	})

	interceptor := NewInterceptor(&InterceptorConfig{TokenFactory: tokenFactory})

	// ht, err := strconv.ParseInt(handlerTimeout, 0, 64)
	// if err != nil {
	// 	return nil, fmt.Errorf("could not parse HANDLER_TIMEOUT as int: %w", err)
	// }

	// timeoutDuration := time.Duration(time.Duration(ht) * time.Second)
	// handler.NewHandler(&handler.Config{
	// 	AccountService:  accountService,
	// 	TokenRepository: tokenRepository,
	// 	TimeoutDuration: time.Duration(time.Duration(ht) * time.Second),
	// })

	return &account.AccountResolversConfig{
		AccountService: accountService,
	}, interceptor, nil
}
