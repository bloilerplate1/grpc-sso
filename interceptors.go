package main

import (
	"context"
	"fmt"
	"log"
	"v-sso/account"
	"v-sso/utilities"
	"v-sso/utilities/errors_handler"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

type Interceptor struct {
	// origin string
	TokenFactory account.ITokenFactory
}

type OperationAccess struct {
	Operation string   `json:"operation"`
	Roles     []string `json:"roles"`
}

type InterceptorConfig struct {
	// origin string
	TokenFactory account.ITokenFactory
}

// type InboundPayload struct {

// }

type InboundMeta struct {
	Key      string
	Required bool
}

var inboundMeta = []*InboundMeta{
	{Key: "api", Required: true},
	{Key: "authorization", Required: false},
}

// var core key = "api"

func NewInterceptor(c *InterceptorConfig) *Interceptor {
	return &Interceptor{
		TokenFactory: c.TokenFactory,
	}
}

func (interceptor *Interceptor) Unary() grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (interface{}, error) {
		log.Println("--> unary interceptor: ", info.FullMethod)

		ctx, err := metaToContext(ctx)

		if err != nil {
			return nil, err
		}

		payload, err := interceptor.authorize(ctx, info.FullMethod)

		if payload != nil {
			ctx = context.WithValue(ctx, "account", payload)
		}

		if err != nil {
			fmt.Println("errrr", err)
			return nil, err
		}

		return handler(ctx, req)
	}
}

func metaToContext(ctx context.Context) (context.Context, error) {
	meta, _ := metadata.FromIncomingContext(ctx)
	// log.Println("md", meta["api"][0])

	for _, v := range inboundMeta {
		if v.Required && (len(meta[v.Key]) < 1) {
			return nil, errors_handler.NewBadRequest(v.Key, "MISSING")
		}
		fmt.Println("v.Key", meta[v.Key][0])
		ctx = context.WithValue(ctx, v.Key, meta[v.Key][0])
	}

	fmt.Println("ctx", ctx)
	return ctx, nil
}

func (it *Interceptor) CreateSSL() (credentials.TransportCredentials, error) {
	creds, err := credentials.NewServerTLSFromFile("./certs/v-sso/server.crt", "./certs/v-sso/server.key")
	// serverCert, err := tls.LoadX509KeyPair("./certs/v-sso/certificate.pem", "./certs/v-sso/key.pem")

	if err != nil {
		return nil, err
	}
	// return credentials.NewTLS(config), nil
	// Create the credentials and return it
	// config := &tls.Config{
	// 	Certificates: []tls.Certificate{serverCert},
	// 	ClientAuth:   tls.NoClientCert,
	// }

	// return credentials.NewTLS(config), nil
	return creds, nil
}

func (interceptor *Interceptor) authorize(ctx context.Context, method string) (*account.AccessTokenPayload, error) {
	access := account.GetOperationAccess(method)

	if access != nil && len(access.Roles) > 0 {
		// everyone can access
		accesstoken := ctx.Value("authorization").(string)

		if len(accesstoken) == 0 {
			return nil, errors_handler.NewInvalidAccessToken()
		}

		payload, err := interceptor.TokenFactory.ValidateAccessToken(accesstoken)

		if err != nil {
			return nil, err
		}

		if len(payload.Role) == 0 {
			return nil, errors_handler.NewUnauthorization("INTERCEPTOR->authorize->EMPTY_ROLE_FROM_ACCESS_TOKEN")
		}

		if !utilities.Contains(access.Roles, payload.Role) {
			return nil, errors_handler.NewUnauthorization("INTERCEPTOR->authorize->INSUFFICIENT_PERMISSIONS")
		}

		fmt.Println("Contains", payload)
		return payload, nil
	}

	return nil, nil
}
