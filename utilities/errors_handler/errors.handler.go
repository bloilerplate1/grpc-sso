package errors_handler

import (
	"fmt"
	"log"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Type string
type Key string

const (
	Authorization        string = "AUTHORIZATION"
	Unauthorization      string = "UNAUTHORIZATION"
	Forbidden            string = "FORBIDDEN"
	BadRequest           string = "BADREQUEST"
	Conflict             string = "CONFLICT"
	Internal             string = "INTERNAL"
	NotFound             string = "NOTFOUND"
	PayloadTooLarge      string = "PAYLOADTOOLARGE"
	UnsupportedMediaType string = "UNSUPPORTEDMEDIATYPE"
	ServiceUnavailable   string = "SERVICE_UNAVAILABLE"
	InvalidAccessToken   string = "INVALID_ACCESS_TOKEN"
	InvalidRefreshToken  string = "INVALID_REFRESH_TOKEN"
	InvalidCredential    string = "INVALID_CREDENTIAL"
)

func NewAuthorization() error {
	return status.Error(codes.AlreadyExists, Internal)
}

func NewUnauthorization(reason string) error {
	fmt.Println(reason)
	return status.Error(codes.PermissionDenied, Unauthorization)
}

func NewForbidden(reason string) error {
	return status.Error(codes.PermissionDenied, Unauthorization)
}

func NewBadRequest(key string, value string) error {
	message := fmt.Sprintf("BadRequest ====> %s is %s", key, value)
	fmt.Println(message)
	return status.Error(codes.InvalidArgument, BadRequest)
}

func NewConflict(name string, value string) error {
	return status.Error(codes.AlreadyExists, Conflict)
}

func NewInternal(reason string) error {
	log.Println(reason)
	return status.Error(codes.Internal, Internal)
}

func NewNotFound(name string, value string) error {
	return status.Error(codes.NotFound, NotFound)
}

func NewPayloadTooLarge(maxBodySize int64, contentLength int64) error {
	return status.Error(codes.ResourceExhausted, PayloadTooLarge)
}

func NewUnsupportedMediaType(reason string) error {
	return status.Error(codes.OutOfRange, Unauthorization)
}

func NewServiceUnavailable() error {
	return status.Error(codes.Unavailable, ServiceUnavailable)
}

func NewValidatorError(reason string) error {
	return status.Error(codes.InvalidArgument, BadRequest)
}

func NewInvalidAccessToken() error {
	return status.Error(codes.PermissionDenied, InvalidAccessToken)
}

func NewInvalidRefreshToken() error {
	return status.Error(codes.PermissionDenied, InvalidRefreshToken)
}

func NewInvalidCredential(reason string) error {
	log.Println(reason)
	return status.Error(codes.Unauthenticated, InvalidCredential)
}
