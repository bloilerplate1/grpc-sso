# Production environment (alias: base)
FROM golang as base
RUN apt update -y && apt install -y bash git
ENV PROTOC_ZIP=protoc-3.13.0-linux-x86_64.zip
RUN apt-get update && apt-get install -y unzip
RUN curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.13.0/$PROTOC_ZIP \
    && unzip -o $PROTOC_ZIP -d /usr/local bin/protoc \
    && unzip -o $PROTOC_ZIP -d /usr/local 'include/*' \ 
    && rm -f $PROTOC_ZIP
WORKDIR /app/v-sso
ENV GO111MODULE=on
COPY . /app/v-sso
# COPY go.sum .
RUN go install github.com/golang/protobuf/protoc-gen-go
RUN go install github.com/favadi/protoc-go-inject-tag
RUN go get -u google.golang.org/grpc
RUN ls /app/v-sso/proto
RUN ldconfig
# Development environment
# Unfortunately, linux alpine doesn't have fswatch package by default, so we will need to download source code and make it by outselves.
FROM base as dev
RUN apt update -y && apt install -y autoconf psmisc automake libtool make g++ fswatch texinfo curl build-essential openssl
WORKDIR /root
RUN wget https://github.com/emcrisostomo/fswatch/releases/download/1.14.0/fswatch-1.14.0.tar.gz
RUN tar -xvzf fswatch-1.14.0.tar.gz
WORKDIR /root/fswatch-1.14.0
RUN ./configure
RUN make
RUN make install
WORKDIR /app/v-sso
# NGINX SERVER ENVIRONMENT
FROM nginx:alpine
COPY --from=admin /app/build /usr/share/nginx/html/v-backoffice
RUN rm /etc/nginx/conf.d/default.conf
ADD ./nginx /etc/nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]