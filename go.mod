module v-sso

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/favadi/protoc-go-inject-tag v1.3.0 // indirect
	github.com/gin-gonic/gin v1.7.4 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.1.2 // indirect
	go.mongodb.org/mongo-driver v1.7.1 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	google.golang.org/grpc v1.39.1 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
