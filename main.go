package main

import (
	"log"
	"net"
	"v-sso/account"
	"v-sso/config"
	"v-sso/datasource"

	"google.golang.org/grpc"
)

type Server struct {
}

func main() {
	log.Println("Hello")
	ds, err := datasource.InitDs()

	if err != nil {
		log.Printf("MONGO ===> Error to connect %s", config.URI)
	}

	go GrpcServer(ds)
	select {}
}

func GrpcServer(ds *datasource.DataSources) error {
	log.Println("GrpcServer LOG")
	dependencies, interceptor, err := inject(ds)

	if err != nil {
		return nil
	}

	lis, err := net.Listen("tcp", config.URI)

	if err != nil {
		log.Printf("grpcServer ===> Error to connect %s", config.URI)
	}

	creds, _ := interceptor.CreateSSL()

	// opts := []grpc.ServerOption{
	// 	grpc.ChainUnaryInterceptor(
	// 		grpc.Creds(creds),
	// 		interceptor.Unary,
	// 	),
	// }

	g := grpc.NewServer(
		grpc.Creds(creds),
		grpc.UnaryInterceptor(interceptor.Unary()),
	)

	account.RegisterAccountServiceServer(g, account.NewAccountResolvers(dependencies))
	g.Serve(lis)

	return nil
}
