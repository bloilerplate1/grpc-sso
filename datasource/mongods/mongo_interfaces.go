package mongods

import (
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoRepository interface {
	GetList() (*mongo.Cursor, error)
	Get(id uuid.UUID) (*mongo.SingleResult, error)
	Create(d interface{}) (bool, error)
	Update(id uuid.UUID, content bson.D) (bool, error)
	Delete(id uuid.UUID) (bool, error)
}
