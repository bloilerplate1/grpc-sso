package datasource

import (
	"context"
	"fmt"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DataSources struct {
	MongoDB *mongo.Client
	context context.Context
}

// initDs is a function for initializing datasource
func InitDs() (*DataSources, error) {
	uri := fmt.Sprintf("%s%s", "mongodb://", os.Getenv("MONGO_HOST"))
	log.Println(uri)
	ctx := context.Background()
	mongoOptions := options.Client().ApplyURI(uri)
	// Connect to MongoDB
	mongoClient, err := mongo.Connect(ctx, mongoOptions)
	if err != nil {
		log.Fatal(err)
	}
	// Check the connection
	err = mongoClient.Ping(ctx, nil)

	if err != nil {
		log.Fatal(err)
	}

	log.Println("Connected to MongoDB!")

	return &DataSources{
		MongoDB: mongoClient,
		context: context.TODO(),
	}, nil
}

func (ds *DataSources) Close() error {
	if err := ds.MongoDB.Disconnect(ds.context); err != nil {
		return fmt.Errorf("error closing Postgresql: %w", err)
	}

	return nil
}
