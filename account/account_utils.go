package account

func ExtractKeyFromAuthorisationTypes(authTypes []*AuthorizationType) []string {
	var keys []string

	for _, auth := range authTypes {
		keys = append(keys, auth.Id)
	}

	return keys
}

func extractRolesName(api string, authorizations []*Authorization) (string, []string) {
	for _, authorization := range authorizations {
		if authorization.ServiceKey == api {
			role := authorization.Role
			permissions := ExtractKeyFromAuthorisationTypes(authorization.Permissions)
			return role.Id, permissions
		}
	}
	return "", []string{}
}
