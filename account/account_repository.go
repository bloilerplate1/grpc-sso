package account

import (
	context "context"
	"fmt"
	"log"
	"time"
	"v-sso/datasource/mongods"
	"v-sso/utilities"
	"v-sso/utilities/errors_handler"

	"github.com/golang/protobuf/ptypes"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type IAccountRepository interface {
	Create(ctx context.Context, u *Account, userCategory string) (string, error)
	FindByID(ctx context.Context, id string) (*Account, error)
	GetList(ctx context.Context, pager *Pager) (*AccountListPayload, error)
	Delete(ctx context.Context, uid string) (bool, error)
	Update(ctx context.Context, uid string, field string, value interface{}) (bool, error)
}

type AccountRepository struct {
	MongoDB           *mongo.Client
	context           context.Context
	accountCollection *mongo.Collection
}

type AccountRepositoryCong struct {
	MongoClient *mongo.Client
}

func NewAccountRepository(c *AccountRepositoryCong) IAccountRepository {
	err := c.MongoClient.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	return &AccountRepository{
		MongoDB:           c.MongoClient,
		context:           context.TODO(),
		accountCollection: c.MongoClient.Database("vdb").Collection("account"),
	}
}

func (ar *AccountRepository) Create(ctx context.Context, acc *Account, userCategory string) (string, error) {
	//defer ar.MongoDB.Disconnect(ar.context)
	pw, err := utilities.HashPassword(acc.Password)
	if err != nil {
		return "", errors_handler.NewBadRequest("PASSWORD", "INVALID")
	}

	t := time.Now()
	ts, err := ptypes.TimestampProto(t)
	acc.Id = acc.Email
	acc.Password = pw
	acc.CreateAt = ptypes.TimestampString(ts)
	acc.UpdateAt = ptypes.TimestampString(ts)
	acc.XTypename = "Account"
	acc.Authorizations = defineDefaultAuthorizations(userCategory)

	//Insert Account to database
	res, err := ar.accountCollection.InsertOne(ar.context, acc)

	if err != nil {
		log.Print("ERROR")
		return "NONE", mongods.MongoErrHandler(err, acc.Id)
	}
	log.Print("SUUCESS")

	return res.InsertedID.(string), nil
}

func (ar *AccountRepository) FindByID(ctx context.Context, id string) (*Account, error) {
	var acc *Account

	filter := bson.M{"_id": id}
	err := ar.accountCollection.FindOne(context.TODO(), filter).Decode(&acc)

	if err != nil {
		return acc, mongods.MongoErrHandler(err, id)
	}

	return acc, nil
}

func (ar *AccountRepository) GetList(ctx context.Context, pager *Pager) (*AccountListPayload, error) {
	total, err := ar.accountCollection.CountDocuments(ctx, bson.D{})

	if err != nil {
		return nil, errors_handler.NewInternal("AccountRepository:GetList:FAIL")
	}

	limit := pager.Limit
	from := pager.From
	var cpt int64
	var accounts []*Account
	opt := options.Find()
	opt.SetLimit(limit)
	opt.SetSkip(from)
	cur, err := ar.accountCollection.Find(ctx, bson.D{{}}, opt)

	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	for cur.Next(context.TODO()) {
		cpt++
		var p Account
		err := cur.Decode(&p)

		if err != nil {
			log.Println(err)
		}

		accounts = append(accounts, &p)
	}

	return &AccountListPayload{
		Total: total,
		Items: accounts,
	}, nil
}

//Delete allow to delete
func (ar *AccountRepository) Delete(ctx context.Context, uid string) (bool, error) {
	res, err := ar.accountCollection.DeleteOne(ctx, bson.M{"_id": uid})

	log.Println(res.DeletedCount)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (ar *AccountRepository) Update(ctx context.Context, uid string, field string, value interface{}) (bool, error) {
	t := time.Now()
	ts, _ := ptypes.TimestampProto(t)
	update := bson.D{{"$set", bson.D{{field, value}, {"update_at", ptypes.TimestampString(ts)}}}}

	_, err := ar.accountCollection.UpdateOne(ctx, bson.M{"_id": uid}, update)

	if err != nil {
		return false, errors_handler.NewInternal("AccountRepository:Update:FAIL")
	}

	return true, nil
}

// import (
// 	"context"
// 	"fmt"
// 	"log"
// 	"time"
// 	"vauth/datasource/mongods"
// 	"vauth/utilities/errors_handler"

// 	"go.mongodb.org/mongo-driver/bson"
// 	"go.mongodb.org/mongo-driver/mongo"
// 	"go.mongodb.org/mongo-driver/mongo/options"
// )

// type AccountRepository struct {
// 	MongoDB *mongo.Client
// 	nameDB  string
// 	context context.Context
// }

// type AccountRepositoryCong struct {
// 	MongoClient *mongo.Client
// }

// type WriteError struct {
// 	Code    int
// 	Index   int
// 	Message string
// }

// type MongoErrors struct {
// 	WriteErrors []WriteError
// }

// type Pager struct {
// 	Limit int64
// 	From  int64
// }

// // type AccountListResponse struct {
// // 	Account []*Account `json:"items"`
// // 	Total   int64          `json:"total"`
// // }

// func (MongoErrors) Error() {
// 	fmt.Println("edd")
// }

// func NewAccountRepository(c *AccountRepositoryCong) IAccountRepository {
// 	err := c.MongoClient.Ping(context.TODO(), nil)

// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	fmt.Println("Connected to MongoDB!")

// 	return &AccountRepository{
// 		MongoDB: c.MongoClient,
// 		nameDB:  "vdb",
// 		context: context.TODO(),
// 	}
// }

// func (ar *AccountRepository) Create(u *Account) (interface{}, error) {
// 	col := ar.MongoDB.Database(ar.nameDB).Collection("account")
// 	//defer ar.MongoDB.Disconnect(ar.context)
// 	now := time.Now()
// 	u.CreateAt = now
// 	u.UpdateAt = now
// 	u.Typename = "Account"
// 	res, err := col.InsertOne(ar.context, u)

// 	if err != nil {
// 		log.Print("ERROR")
// 		return "NONE", mongods.MongoErrHandle(err, u.UID)
// 	}
// 	log.Print("SUUCESS")

// 	return res.InsertedID, nil
// }

// func (ar *AccountRepository) FindByID(id string) (*Account, error) {
// 	var acc *Account

// 	col := ar.MongoDB.Database(ar.nameDB).Collection("account")

// 	filter := bson.M{"_id": id}
// 	err := col.FindOne(context.TODO(), filter).Decode(&acc)

// 	if err != nil {
// 		return acc, mongods.MongoErrHandle(err, id)
// 	}

// 	return acc, nil
// }

// func (ur *AccountRepository) Update(ctx context.Context, uid string, field string, value interface{}) error {
// 	col := ur.MongoDB.Database(ur.nameDB).Collection("account")

// 	update := bson.D{{"$set", bson.D{{field, value}, {"UPDATE_AT", time.Now()}}}}

// 	_, err := col.UpdateOne(ctx, bson.M{"_id": uid}, update)

// 	if err != nil {
// 		return errors_handler.NewInternal("AccountRepository:Update:FAIL")
// 	}
// 	return nil
// }

// func (ur *AccountRepository) UpdateAccount(ctx context.Context, uid string, value *FormUpdate) error {
// 	col := ur.MongoDB.Database(ur.nameDB).Collection("account")
// 	// opts := options.Update().SetUpsert(true)

// 	update := bson.D{{Key: "$set", Value: value}}
// 	upsert := true
// 	after := options.After

// 	opts := options.FindOneAndUpdateOptions{
// 		ReturnDocument: &after,
// 		Upsert:         &upsert,
// 	}

// 	result := col.FindOneAndUpdate(ctx, bson.M{"_id": uid}, update, &opts)

// 	if result.Err() != nil {
// 		return result.Err()
// 	}
// 	// _, err = mongods.UpdateResponse(result)

// 	return nil
// }

// //GetList retrieve list
// func (ur *AccountRepository) GetList(ctx context.Context, pager Pager) (*AccountListResponse, error) {
// 	col := ur.MongoDB.Database(ur.nameDB).Collection("account")
// 	total, err := col.CountDocuments(ctx, bson.D{})

// 	if err != nil {
// 		return nil, errors_handler.NewInternal("AccountRepository:GetList:FAIL")
// 	}

// 	limit := pager.Limit
// 	from := pager.From
// 	var cpt int64
// 	var accounts []*Account
// 	opt := options.Find()
// 	opt.SetLimit(limit)
// 	opt.SetSkip(from)
// 	cur, err := col.Find(ctx, bson.D{{}}, opt)
// 	if err != nil {
// 		log.Fatal(err)
// 		return nil, err
// 	}

// 	for cur.Next(context.TODO()) {
// 		cpt++
// 		var p Account
// 		err := cur.Decode(&p)
// 		if err != nil {
// 			log.Println(err)
// 		}

// 		accounts = append(accounts, &p)
// 	}

// 	return &AccountListResponse{
// 		Account: accounts,
// 		Total:   total,
// 	}, nil
// }

// //Delete allow to delete
// func (ur *AccountRepository) Delete(ctx context.Context, uid string) error {
// 	col := ur.MongoDB.Database(ur.nameDB).Collection("account")

// 	res, err := col.DeleteOne(ctx, bson.M{"_id": uid})

// 	log.Println(res.DeletedCount)
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

// // func isMongoDupKey(err error) bool {
// // 	wce, ok := err.(MongoErrors)
// // 	log.Println(err)
// // 	if !ok {
// // 		return false
// // 	}
// // 	return wce.Code == 11000 || wce.Code == 11001 || wce.Code == 12582 || wce.Code == 16460 && strings.Contains(wce.Message, " E11000 ")
// // }

// // func mongoError(errs error) {
// // 	strErrors := make([]string, len(errs.Error()))

// // 	for i, err := range errs.Error() {
// // 		strErrors[i] = err.Error()
// // 	}
// // }
