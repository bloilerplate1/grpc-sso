package account

// import (
// 	"context"
// 	"fmt"
// 	"log"

// 	"vauth/utilities"
// 	"vauth/utilities/errors_handler"
// )

// type AccountService struct {
// 	AccountRepository IAccountRepository
// 	TokenRepository   ITokenRepository
// }

// // USConfig will hold repositories that will eventually be injected into this
// // this service layer
// type AccountServiceConfig struct {
// 	AccountRepository IAccountRepository
// 	TokenRepository   ITokenRepository
// }

// type FormUpdate struct {
// 	Username string `json:"username" bson:"username"`
// }

// // NewAccountService is a factory function for
// // initializing a AccountService with its repository layer dependencies
// func NewAccountService(c *AccountAccountConfig) IAccountService {
// 	return &AccountService{
// 		AccountRepository: c.AccountRepository,
// 		TokenRepository:   c.TokenRepository,
// 	}
// }

// func (s *AccountService) Search(ctx context.Context, pager Pager) (*AccountListResponse, error) {
// 	res, err := s.AccountRepository.GetList(ctx, pager)

// 	if err != nil {
// 		return nil, err
// 	}

// 	return res, nil
// }

// func (s *AccountService) Get(ctx context.Context, uid string) (*Account, error) {
// 	return s.AccountRepository.FindByID(uid)
// }

// func (s *AccountService) Signup(ctx context.Context, u *Account) (interface{}, *Session, error) {
// 	pw, err := utilities.HashPassword(u.Password)
// 	accountCategory := "ADMIN"

// 	if err != nil {
// 		log.Printf("Unable to signup account for email: %v\n", u.Email)
// 		return nil, nil, errors_handler.NewInternal("AccountService::Signup::FAIL_TO_HASH_PASSWORD")
// 	}

// 	u.Password = pw
// 	u.UID = u.Email
// 	u.Authorizations = defineDefaultAuthorizations(accountCategory)
// 	tokens, _ := s.TokenRepository.NewPairFromAccount(ctx, u, "")
// 	u.RefreshToken = tokens.RefreshToken
// 	u.Blacklisted = false
// 	uid, err := s.AccountRepository.Create(u)

// 	if err != nil {
// 		return nil, nil, err
// 	}

// 	return uid, tokens, nil
// }

// const (
// 	ADMIN_ACCOUNT string = "ADMIN_ACCOUNT"
// )

// func (s *AccountService) Signin(ctx context.Context, u *Account) (string, *Session, error) {
// 	account, err := s.AccountRepository.FindByID(u.Email)
// 	// Will return NotAuthorized to client to omit details of why
// 	if err != nil {
// 		return "", nil, err
// 	}
// 	// verify password - we previously created this method
// 	match, err := utilities.ComparePasswords(account.Password, u.Password)

// 	if err != nil {
// 		return "", nil, errors_handler.NewInternal("AccountService::Signin::")
// 	}

// 	if !match {
// 		return "", nil, errors_handler.NewInvalidAccessToken()
// 	}

// 	tokens, _ := s.TokenRepository.NewPairFromAccount(ctx, account, "")

// 	err = s.AccountRepository.Update(ctx, account.UID, "refreshToken", tokens.RefreshToken)

// 	if err != nil {
// 		return "", nil, err
// 	}

// 	return account.UID, tokens, nil
// }

// func (s *AccountService) Update(ctx context.Context, uid string, form *FormUpdate) error {
// 	return s.AccountRepository.UpdateAccount(ctx, uid, form)
// }

// func (s *AccountService) NewPairFromAccount(ctx context.Context, u *Account) (*Session, error) {
// 	return s.TokenRepository.NewPairFromAccount(ctx, u, "")
// }

// func (s *AccountService) GetAccountAuthorizaions(uid string) ([]*Authorization, error) {
// 	account, err := s.AccountRepository.FindByID(uid)

// 	if err != nil {
// 		return nil, err
// 	}

// 	return account.Authorizations, nil

// }

// func (s *AccountService) RefreshAccessToken(ctx context.Context, refreshToken string) (*Session, error) {
// 	rt, err := s.TokenRepository.ValidateRefreshToken(refreshToken)

// 	if err != nil {
// 		fmt.Println("TOKEN_PARSE_ERROR")
// 		return nil, errors_handler.NewInvalidRefreshToken()
// 	}

// 	uid := rt.UID
// 	account, err := s.AccountRepository.FindByID(uid)

// 	if err != nil || account.RefreshToken != refreshToken {
// 		fmt.Println("TOKEN_MATCH_ERROR")
// 		return nil, errors_handler.NewInvalidRefreshToken()
// 	}

// 	accessToken, err := s.TokenRepository.RefreshAccessToken(ctx, account)

// 	if err != nil {
// 		log.Printf("Error generating idToken for uid: %v. Error: %v\n", uid, err.Error())
// 		return nil, errors_handler.NewInternal("")
// 	}

// 	// err = s.AccountRepository.Update(ctx, uid, accessToken.SS)

// 	// if err != nil {
// 	// 	return nil, errors_handler.NewInternal()
// 	// }

// 	return &Session{
// 		ID:            id,
// 		Authenticated: true,
// 		AccessToken:   accessToken.SS,
// 		RefreshToken:  account.RefreshToken,
// 	}, nil
// 	//return s.AccountRepository.FindByID(uid)
// }

// func (s *AccountService) BlacklistAccount(ctx context.Context, uid string, value bool) bool {
// 	err := s.AccountRepository.Update(ctx, uid, "blacklisted", value)

// 	return err == nil
// }

// func (s *AccountService) Delete(ctx context.Context, uid string) error {
// 	err := s.AccountRepository.Delete(ctx, uid)

// 	if err != nil {
// 		log.Panicln("err")
// 		log.Println(err)
// 		return errors_handler.NewInternal("AccountService::Delete::FAIL_TO_DELETE_ACCOUNT")
// 	}

// 	return nil
// }

// func (s *AccountService) RevokeRefreshToken(ctx context.Context, uid string) bool {
// 	err := s.AccountRepository.Update(ctx, uid, "refreshToken", nil)
// 	fmt.Println(err)
// 	return err == nil
// }

// func (s *AccountService) getServiceAuthorizations(ctx context.Context, uid string, service string) (*Account, *Authorization, error) {
// 	account, err := s.AccountRepository.FindByID(uid)
// 	if err != nil {
// 		return nil, nil, err
// 	}

// 	return account, extractAuthorizations(account.Authorizations, service), nil
// }

// func extractAuthorizations(roles []*Authorization, service string) *Authorization {
// 	for _, role := range roles {
// 		// rolesName = append(rolesName, role.Id)
// 		if role.ServiceKey == service {
// 			return role
// 		}
// 	}
// 	return &Authorization{}
// }
