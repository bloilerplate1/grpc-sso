package account

import (
	context "context"
	"v-sso/utilities"
	"v-sso/utilities/errors_handler"
)

type IAccountService interface {
	Create(ctx context.Context, acc *Account, userCategory string) (string, error)
	FindByID(ctx context.Context, id string) (*Account, error)
	GetList(ctx context.Context, pager *Pager) (*AccountListPayload, error)
	Delete(ctx context.Context, uid string) (bool, error)
	Update(ctx context.Context, uid string, field string, value interface{}) (bool, error)
	GetSession(ctx context.Context, tokens *TokenPair) (*Session, error)
	Signin(ctx context.Context, credentials *AccountCredentials) (*Session, error)
	GetAccountAuthorizations(ctx context.Context, uid string) ([]*Authorization, error)
}

type AccountService struct {
	AccountRepository IAccountRepository
	TokenFactory      ITokenFactory
}

type AccountServiceConfig struct {
	AccountRepository IAccountRepository
	TokenFactory      ITokenFactory
}

func NewAccountService(c *AccountServiceConfig) *AccountService {
	return &AccountService{
		AccountRepository: c.AccountRepository,
		TokenFactory:      c.TokenFactory,
	}
}

func (service *AccountService) Create(ctx context.Context, acc *Account, userCategory string) (string, error) {
	return service.AccountRepository.Create(ctx, acc, userCategory)
}

func (service *AccountService) FindByID(ctx context.Context, id string) (*Account, error) {
	return service.AccountRepository.FindByID(ctx, id)
}

func (service *AccountService) GetList(ctx context.Context, pager *Pager) (*AccountListPayload, error) {
	return service.AccountRepository.GetList(ctx, pager)
}

func (service *AccountService) Delete(ctx context.Context, uid string) (bool, error) {
	return service.AccountRepository.Delete(ctx, uid)
}

func (service *AccountService) Update(ctx context.Context, uid string, field string, value interface{}) (bool, error) {
	return service.AccountRepository.Update(ctx, uid, field, value)
}

func (service *AccountService) GetSession(ctx context.Context, tokens *TokenPair) (*Session, error) {
	payload, err := service.TokenFactory.ValidateAccessToken(tokens.AccessToken)

	if err != nil {
		uid, err := service.TokenFactory.ValidateRefreshToken(tokens.RefreshToken)

		if err != nil {
			return nil, err
		}

		account, err := service.AccountRepository.FindByID(ctx, uid)

		if err != nil {
			return nil, err
		}

		api, found := ctx.Value("api").(string)

		if !found {
			return nil, errors_handler.NewInternal("AccountService->GetSession->API_NOT_SET_TO_CONTEXT")
		}

		role, _ := extractRolesName(api, account.Authorizations)

		status := SessionStatus[role]

		accessToken, err := service.TokenFactory.RefreshAccessToken(ctx, account)

		if err != nil {
			return nil, err
		}

		return &Session{
			Id:            account.Id,
			Username:      account.Username,
			Authenticated: true,
			Status:        status,
			Email:         account.Email,
			AccessToken:   accessToken,
			RefreshToken:  tokens.RefreshToken,
			XTypename:     "Session",
		}, nil
	}

	if payload == nil {
		return nil, errors_handler.NewInvalidAccessToken()
	}

	return &Session{
		Id:            payload.UID,
		Username:      payload.Username,
		Email:         payload.Email,
		Role:          payload.Role,
		Authenticated: true,
		Status:        SessionStatus[payload.Role],
		RefreshToken:  tokens.RefreshToken,
		AccessToken:   tokens.AccessToken,
		XTypename:     "Session",
	}, nil
}

func (service *AccountService) Signin(ctx context.Context, credentials *AccountCredentials) (*Session, error) {
	account, err := service.AccountRepository.FindByID(ctx, credentials.Email)

	if err != nil {
		return nil, err
	}

	match, err := utilities.ComparePasswords(account.Password, credentials.Password)

	if err != nil {
		return nil, err
	}

	if !match {
		return nil, errors_handler.NewInvalidCredential("INVALID_PASSWORD")
	}

	session, err := service.TokenFactory.NewPairFromAccount(ctx, account, "")

	if err != nil {
		return nil, err
	}

	_, err = service.TokenFactory.SetRefreshToken(ctx, account.Id, session.RefreshToken)

	if err != nil {
		return nil, err
	}

	return session, nil
}

func (service *AccountService) GetAccountAuthorizations(ctx context.Context, uid string) ([]*Authorization, error) {
	account, err := service.AccountRepository.FindByID(ctx, uid)

	if err != nil {
		return nil, err
	}

	return account.Authorizations, nil

}
