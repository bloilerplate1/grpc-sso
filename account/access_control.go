package account

import "strings"

type OperationAccess struct {
	Operation string   `json:"operation"`
	Roles     []string `json:"roles"`
}

var AccessStore = map[string]*OperationAccess{
	"CreateAdminAccount": {
		Roles: []string{"ADMIN"},
	},
	"ValidateAccessToken": {
		Roles: []string{"ADMIN", "SUBSCRIBER", "PRO"},
	},
	"GetAccount": {
		Roles: []string{"ADMIN"},
	},
	"UpdateAccount": {
		Roles: []string{"ADMIN"},
	},
	"DeleteAccount": {
		Roles: []string{"ADMIN"},
	},
	"SearchAccounts": {
		Roles: []string{"ADMIN"},
	},
	"BlacklistAccount": {
		Roles: []string{"ADMIN"},
	},
	"SearchAccountAuthorizations": {
		Roles: []string{"ADMIN"},
	},
	"UpdateAccountAuthorization": {
		Roles: []string{"ADMIN"},
	},
}

var SessionStatus = map[string]string{
	"ADMIN": "ADMIN_SESSION_ENABLED",
	"PRO":   "PRO_SESSION_ENABLED",
}

func GetOperationAccess(fullmethod string) *OperationAccess {
	result := strings.Split(fullmethod, "/")
	operation := result[2]

	return AccessStore[operation]
}
