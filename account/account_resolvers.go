package account

import (
	context "context"
	"log"
	"v-sso/utilities"
)

type AccountResolvers struct {
	AccountService IAccountService
}

type AccountResolversConfig struct {
	AccountService IAccountService
}

func NewAccountResolvers(c *AccountResolversConfig) *AccountResolvers {
	return &AccountResolvers{
		AccountService: c.AccountService,
	}
}

func (resolver *AccountResolvers) GetAuthorizationTypeList(ctx context.Context, a *AuthorizationTypeRequest) (*AuthorizationListResponse, error) {
	if a.Name == "PERMISSION" {
		return &AuthorizationListResponse{
			Payload: &AuthorizationListPayload{
				Reason: "Hello",
				Total:  int32(len(PermissionsType)),
				Items:  PermissionsType,
			},
		}, nil
	} else {
		return &AuthorizationListResponse{
			Payload: &AuthorizationListPayload{
				Reason: "Hello",
				Total:  int32(len(RolesType)),
				Items:  RolesType,
			},
		}, nil
	}
}

func (resolver *AccountResolvers) CreateAccount(ctx context.Context, acc *Account) (*StringResponse, error) {
	id, err := resolver.AccountService.Create(ctx, acc, Admin)

	if err != nil {
		return nil, err
	}

	return &StringResponse{
		Payload: id,
	}, nil
}

func (resolver *AccountResolvers) GetAccount(ctx context.Context, request *ID) (*AccountResponse, error) {
	acc, err := resolver.AccountService.FindByID(ctx, request.Id)

	if err != nil {
		return nil, err
	}

	return &AccountResponse{
		Payload: acc,
	}, nil
}

func (resolver *AccountResolvers) GetBasicAccount(context.Context, *ID) (*BasicAccountResponse, error) {
	return &BasicAccountResponse{
		Payload: &BasicAccount{
			Id:          "doudou.leydi.fall@gmail.com",
			Username:    "doudou",
			Email:       "doudou.leydi.fall@gmail.com",
			Blacklisted: true,
			XTypename:   "Account",
		},
	}, nil
}

func (s *AccountResolvers) UpdateAccount(context.Context, *AccountUpdateForm) (*BooleanResponse, error) {
	return &BooleanResponse{
		Payload: true,
	}, nil
}

func (resolver *AccountResolvers) DeleteAccount(ctx context.Context, request *ID) (*BooleanResponse, error) {
	success, err := resolver.AccountService.Delete(ctx, request.Id)

	if err != nil {
		return nil, err
	}

	return &BooleanResponse{
		Payload: success,
	}, nil
}

func (resolver *AccountResolvers) GetSession(ctx context.Context, tokens *TokenPair) (*SessionResponse, error) {
	log.Println("tokens", tokens)
	session, err := resolver.AccountService.GetSession(ctx, tokens)

	if err != nil {
		return nil, err
	}

	return &SessionResponse{
		Payload: session,
	}, nil
}

func (s *AccountResolvers) RevokeRefeshToken(context.Context, *ID) (*BooleanResponse, error) {
	return &BooleanResponse{
		Payload: true,
	}, nil
}

func (s *AccountResolvers) RefeshAccessToken(context.Context, *RefreshTokenRequest) (*SessionResponse, error) {
	return &SessionResponse{
		Payload: &Session{
			Id:           "doudou.leydi.fall@gmail.com",
			Username:     "doudou",
			Email:        "doudou.leydi.fall@gmail.com",
			AccessToken:  "cndkvnedkjvndsvndfsklvnfdsgdfgdsg",
			RefreshToken: "fsdfnsdkjfdsghuetryerwyeyeyeywwe",
		},
	}, nil
}

func (s *AccountResolvers) SearchAccounts(ctx context.Context, request *Pager) (*AccountListResponse, error) {
	acc, err := s.AccountService.GetList(ctx, request)

	if err != nil {
		return nil, err
	}

	return &AccountListResponse{
		Payload: acc,
	}, nil
}

func (resolver *AccountResolvers) Signin(ctx context.Context, request *AccountCredentials) (*SessionResponse, error) {
	session, err := resolver.AccountService.Signin(ctx, request)

	if err != nil {
		return nil, err
	}

	return &SessionResponse{
		Payload: session,
	}, nil
}

func (s *AccountResolvers) Signout(context.Context, *ID) (*BooleanResponse, error) {
	return &BooleanResponse{
		Payload: true,
	}, nil
}

func (s *AccountResolvers) Register(context.Context, *RegisterForm) (*SessionResponse, error) {
	return &SessionResponse{
		Payload: &Session{
			Id:            "doudou.leydi.fall@gmail.com",
			Username:      "doudou",
			Email:         "doudou.leydi.fall@gmail.com",
			Authenticated: true,
			AccessToken:   "cndkvnedkjvndsvndfsklvnfdsgdfgdsg",
			RefreshToken:  "fsdfnsdkjfdsghuetryerwyeyeyeywwe",
		},
	}, nil
}

func (s *AccountResolvers) CreateAdminAccount(ctx context.Context, account *Account) (*StringResponse, error) {
	id, err := s.AccountService.Create(ctx, account, Admin)

	if err != nil {
		return nil, err
	}

	return &StringResponse{
		Payload: id,
	}, nil
}

func (s *AccountResolvers) BlacklistAccount(context.Context, *BlacklistAccountRequest) (*BooleanResponse, error) {
	return &BooleanResponse{
		Payload: true,
	}, nil
}

func (s *AccountResolvers) ChangePassword(ctx context.Context, request *AccountCredentials) (*BooleanResponse, error) {
	password, _ := utilities.HashPassword(request.Password)
	changed, err := s.AccountService.Update(ctx, request.Email, "password", password)

	if err != nil {
		return nil, err
	}

	return &BooleanResponse{
		Payload: changed,
	}, nil
}

func (service *AccountResolvers) SearchAccountAuthorizations(ctx context.Context, request *ID) (*AuthorizationsListResponse, error) {
	authorizations, err := service.AccountService.GetAccountAuthorizations(ctx, request.Id)

	if err != nil {
		return nil, err
	}

	return &AuthorizationsListResponse{
		Payload: &AuthorizationsListPayload{
			Total:  int32(len(authorizations)),
			Reason: "",
			Items:  authorizations,
		},
	}, nil
}

func (s *AccountResolvers) GetAccountAuthorization(ctx context.Context, request *AccountAuthorizationRequest) (*BooleanResponse, error) {
	return &BooleanResponse{
		Payload: true,
	}, nil
}

func (s *AccountResolvers) UpdateAccountAuthorization(context.Context, *AuthorizationForm) (*BooleanResponse, error) {
	return &BooleanResponse{
		Payload: true,
	}, nil
}
