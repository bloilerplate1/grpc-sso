package account

const (
	Admin        string = "ADMIN"
	Professional string = "PRO"
	Subscriber   string = "SUBSCRIBER"
)

type Service struct {
	ServiceKey  string
	ServiceName string
}

const accountServicePath = "/account.AccountService/"

var AuthorizationAccess = map[string][]string{
	accountServicePath + "CreateAdminAccount": {Admin},
}

var Core = Service{ServiceKey: "core", ServiceName: "core"}

var RolesType = []*AuthorizationType{
	{Id: "ADMIN", Name: "Administrator", XTypename: "AuthorizationType"},
	{Id: "SUBSCRIBER", Name: "Subscriber", XTypename: "AuthorizationType"},
	{Id: "PRO", Name: "Professional", XTypename: "AuthorizationType"},
}

var PermissionsType = []*AuthorizationType{
	{Id: "read:any_account", Name: "read:any_account", XTypename: "AuthorizationType"},
	{Id: "read:own_account", Name: "read:own_account", XTypename: "AuthorizationType"},
	{Id: "delete:any_account", Name: "delete:any_account", XTypename: "AuthorizationType"},
}

// ADMIN AUTHORIZATIONS ======

var AdminDefaultRole = &AuthorizationType{Id: "ADMIN", Name: "Administrator", XTypename: "AuthorizationType"}

var AdminDefaultRoleCore = &AuthorizationType{Id: "ADMIN", Name: "Administrator", XTypename: "AuthorizationType"}

var AdminDefaultPermissionsCore = []*AuthorizationType{
	{Id: "read:any_account", Name: "read:any_account", XTypename: "AuthorizationType"},
	{Id: "read:own_account", Name: "read:own_account", XTypename: "AuthorizationType"},
	{Id: "delete:any_account", Name: "delete:any_account", XTypename: "AuthorizationType"},
}

var AdminDefaultAuthorizationCore = &Authorization{
	ServiceKey:  Core.ServiceKey,
	ServiceName: Core.ServiceName,
	Permissions: AdminDefaultPermissionsCore,
	Role:        AdminDefaultRoleCore,
	XTypename:   "Authorization",
}

func setAdminDefaultAuthorizations() []*Authorization {
	authorizations := []*Authorization{
		AdminDefaultAuthorizationCore,
	}

	return authorizations
}

// PRO AUTHORIZATIONS ======

var ProDefaultRoles = AuthorizationType{Id: "ADMIN", Name: "Administrator", XTypename: "AuthorizationType"}

var ProDefaultRoleCore = &AuthorizationType{Id: "PRO", Name: "Professional", XTypename: "AuthorizationType"}

var ProDefaultPermissionsCore = []*AuthorizationType{
	{Id: "read:own_account", Name: "read:own_account", XTypename: "AuthorizationType"},
}

var ProDefaultAuthorizationCore = &Authorization{
	ServiceKey:  Core.ServiceKey,
	ServiceName: Core.ServiceName,
	Permissions: ProDefaultPermissionsCore,
	Role:        ProDefaultRoleCore,
	XTypename:   "Authorization",
}

func setProDefaultAuthorizations() []*Authorization {
	authorizations := []*Authorization{
		ProDefaultAuthorizationCore,
	}

	return authorizations
}

// SUBSCRIBER AUTHORIZATIONS ======

var SubscriberDefaultRole = AuthorizationType{Id: "ADMIN", Name: "Administrator", XTypename: "AuthorizationType"}

var SubscriberDefaultRoleCore = &AuthorizationType{Id: "PRO", Name: "Professional", XTypename: "AuthorizationType"}

var SubscriberDefaultPermissionsCore = []*AuthorizationType{
	{Id: "read:own_account", Name: "read:own_account", XTypename: "AuthorizationType"},
}

var SubscriberDefaultAuthorizationCore = &Authorization{
	ServiceKey:  Core.ServiceKey,
	ServiceName: Core.ServiceName,
	Permissions: SubscriberDefaultPermissionsCore,
	Role:        SubscriberDefaultRoleCore,
	XTypename:   "Authorization",
}

func setSubscriberDefaultAuthorizations() []*Authorization {
	authorizations := []*Authorization{
		AdminDefaultAuthorizationCore,
	}

	return authorizations
}

// UTILITIES AUTHORIZATIONS ======

func defineDefaultAuthorizations(userCategory string) []*Authorization {
	switch userCategory {
	case Admin:
		return setAdminDefaultAuthorizations()
	case Professional:
		return setProDefaultAuthorizations()
	case Subscriber:
		return setSubscriberDefaultAuthorizations()
	default:
		return []*Authorization{}
	}
}
