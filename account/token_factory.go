package account

import (
	"context"
	"crypto/rsa"
	"fmt"
	"log"
	"strings"
	"time"
	"v-sso/utilities/errors_handler"

	"github.com/dgrijalva/jwt-go"
	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type ITokenFactory interface {
	NewPairFromAccount(ctx context.Context, u *Account, prevTokenID string) (*Session, error)
	RefreshAccessToken(ctx context.Context, u *Account) (string, error)
	ValidateAccessToken(tokenString string) (*AccessTokenPayload, error)
	ValidateRefreshToken(tokenString string) (string, error)
	SetRefreshToken(ctx context.Context, uid string, token string) (bool, error)
}

type AccessTokenPayload struct {
	API             string   `json:"api"`
	UID             string   `json:"uid"`
	Email           string   `json:"email"`
	Username        string   `json:"username"`
	Role            string   `json:"roles"`
	Permissions     []string `json:"permissions"`
	AccountProvider string   `json:"provider"`
}

type accessTokenCustomClaims struct {
	Payload *AccessTokenPayload
	jwt.StandardClaims
}

type refreshTokenCustomClaims struct {
	UID string `json:"uid"`
	jwt.StandardClaims
}

// type AccessToken struct {
// 	SS string `json:"accessToken" bson:"accessToken"`
// }

type RefreshToken struct {
	ID  uuid.UUID `json:"-"`
	UID string    `json:"-"`
	SS  string    `json:"refreshToken" bson:"refreshToken"`
}

type TokenFactoryConfig struct {
	TokenFactory          ITokenFactory
	PrivKey               *rsa.PrivateKey
	PubKey                *rsa.PublicKey
	RefreshSecret         string
	IDExpirationSecs      int64
	RefreshExpirationSecs int64
	MongoClient           *mongo.Client
	tokenCollection       *mongo.Collection
}

type tokenFactory struct {
	PrivKey               *rsa.PrivateKey
	PubKey                *rsa.PublicKey
	RefreshSecret         string
	IDExpirationSecs      int64
	RefreshExpirationSecs int64
	MongoDB               *mongo.Client
	nameDB                string
	context               context.Context
	tokenCollection       *mongo.Collection
}

// NewTokenRepository is a factory for initializing Account Repositories
func NewTokenFactory(c *TokenFactoryConfig) ITokenFactory {
	return &tokenFactory{
		PrivKey:               c.PrivKey,
		PubKey:                c.PubKey,
		RefreshSecret:         c.RefreshSecret,
		IDExpirationSecs:      c.IDExpirationSecs,
		RefreshExpirationSecs: c.RefreshExpirationSecs,
		MongoDB:               c.MongoClient,
		nameDB:                "vdb",
		context:               context.TODO(),
		tokenCollection:       c.MongoClient.Database("vdb").Collection("token"),
	}
}

// NewPairFromAccount creates fresh id and refresh tokens for the current Account
// If a previous token is included, the previous token is removed from
// the tokens repository
func (s *tokenFactory) NewPairFromAccount(ctx context.Context, account *Account, prevTokenID string) (*Session, error) {
	api, found := ctx.Value("api").(string)

	if !found {
		return nil, errors_handler.NewInternal("tokenFactory->NewPairFromAccount->API_MISSING_FROM_CONTEXT")
	}

	role, _ := extractRolesName(api, account.Authorizations)

	status := SessionStatus[role]

	accessToken, err := generateAccessToken(account, api, s.PrivKey, s.IDExpirationSecs)

	if err != nil {
		return nil, errors_handler.NewInternal(fmt.Sprintf("Error generating idToken for uid: %v. Error: %v\n", account.Id, err.Error()))
	}

	refreshToken, err := generateRefreshToken(account.Id, s.RefreshSecret, s.RefreshExpirationSecs)

	if err != nil {
		return nil, errors_handler.NewInternal(fmt.Sprintf("Error generating refreshToken for uid: %v. Error: %v\n", account.Id, err.Error()))
	}

	return &Session{
		Id:            account.Id,
		Username:      account.Username,
		Email:         account.Email,
		Authenticated: true,
		Status:        status,
		AccessToken:   accessToken,
		RefreshToken:  refreshToken,
		XTypename:     "Session",
	}, nil
}

func (s *tokenFactory) RefreshAccessToken(ctx context.Context, u *Account) (string, error) {
	api, found := ctx.Value("api").(string)

	if !found {
		return "", errors_handler.NewInternal("tokenRepository::RefreshAccessToken::API_NOT_SET_TO_CONTEXT")
	}

	accessToken, err := generateAccessToken(u, api, s.PrivKey, s.IDExpirationSecs)

	if err != nil {
		return "", errors_handler.NewInternal(fmt.Sprintf("Error generating idToken for uid: %v. Error: %v\n", u.Id, err.Error()))
	}

	return accessToken, nil
}

// validateAccessToken validates the id token jwt string
// It returns the account extract from the accessTokenCustomClaims
func (s *tokenFactory) ValidateAccessToken(tokenString string) (*AccessTokenPayload, error) {
	fullAccessToken := strings.Split(tokenString, "Bearer ")

	if len(fullAccessToken) < 2 {
		return nil, errors_handler.NewInvalidAccessToken()
	}

	claims, err := validateAccessToken(fullAccessToken[1], s.PubKey) // uses public RSA key

	// We'll just return unauthorized error in all instances of failing to verify account
	if err != nil {
		log.Printf("INVALID_ACCESS_TOKEN %v\n", err)
		return nil, errors_handler.NewInvalidAccessToken()
	}

	return claims.Payload, nil
}

// ValidateRefreshToken checks to make sure the JWT provided by a string is valid
// and returns a RefreshToken if valid
func (s *tokenFactory) ValidateRefreshToken(tokenString string) (string, error) {
	fullRefreshToken := strings.Split(tokenString, "Bearer ")

	if len(fullRefreshToken) < 2 {
		return "", errors_handler.NewInvalidAccessToken()
	}

	// validate actual JWT with string a secret
	claims, err := validateRefreshToken(fullRefreshToken[1], s.RefreshSecret)

	// We'll just return unauthorized error in all instances of failing to verify account
	if err != nil {
		log.Printf("Unable to validate or parse refreshToken for token string: %s\n%v\n", tokenString, err)
		return "", errors_handler.NewInvalidRefreshToken()
	}

	// Standard claims store ID as a string. I want "model" to be clear our string
	// is a UUID. So we parse claims.Id as UUID
	// tokenUUID, err := uuid.Parse(claims.Id)

	if err != nil {
		log.Printf("INVALID_REFRESH_TOKEN %s\n%v\n", claims.Id, err)
		return "", errors_handler.NewInvalidRefreshToken()
	}

	return claims.UID, nil
}

// SetRefreshToken stores a refresh token with an expiry time
func (tr *tokenFactory) SetRefreshToken(ctx context.Context, uid string, token string) (bool, error) {
	t := time.Now()
	ts, _ := ptypes.TimestampProto(t)

	update := bson.D{{"$set", bson.D{{Key: "refreshToken", Value: token}, {"update_at", ptypes.TimestampString(ts)}}}}

	_, err := tr.tokenCollection.UpdateOne(ctx, bson.M{"_id": uid}, update)

	if err != nil {
		return false, errors_handler.NewInternal("AccountRepository:Update:FAIL")
	}

	return true, nil
}

// DeleteRefreshToken used to delete old  refresh tokens
// Services my access this to revolve tokens
func (r *tokenFactory) DeleteRefreshToken(ctx context.Context, accountID string, tokenID string) error {
	return nil
}

// generateIDToken generates an IDToken which is a jwt with myCustomClaims
// Could call this GenerateIDTokenString, but the signature makes this fairly clear
func generateAccessToken(u *Account, api string, key *rsa.PrivateKey, exp int64) (string, error) {
	unixTime := time.Now().Unix()
	tokenExp := unixTime + exp // 15 minutes from current time
	role, permissions := extractRolesName(api, u.Authorizations)
	claims := accessTokenCustomClaims{
		Payload: &AccessTokenPayload{
			API:         api,
			Email:       u.Email,
			UID:         u.Id,
			Username:    u.Username,
			Role:        role,
			Permissions: permissions,
		},
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  unixTime,
			ExpiresAt: tokenExp,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	ss, err := token.SignedString(key)

	if err != nil {
		log.Println("Failed to sign id token string")
		return "", err
	}

	return fmt.Sprintf("%s %s", "Bearer", ss), nil
}

// generateRefreshToken creates a refresh token
// The refresh token stores only the Account's ID, a string
func generateRefreshToken(accountID string, key string, exp int64) (string, error) {
	currentTime := time.Now()
	tokenExp := currentTime.Add(time.Duration(exp) * time.Second) // 3 days
	tokenID, err := uuid.NewRandom()                              // v4 uuid in the google uuid lib

	if err != nil {
		log.Println("Failed to generate refresh token ID")
		return "", err
	}

	claims := refreshTokenCustomClaims{
		UID: accountID,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  currentTime.Unix(),
			ExpiresAt: tokenExp.Unix(),
			Id:        tokenID.String(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString([]byte(key))

	if err != nil {
		log.Println("Failed to sign refresh token string")
		return "", err
	}

	return fmt.Sprintf("%s %s", "Bearer", ss), nil

	// return &refreshTokenData{
	// 	SS:        ss,
	// 	ID:        tokenID,
	// 	ExpiresIn: tokenExp.Sub(currentTime),
	// }, nil
}

// validateRefreshToken uses the secret key to validate a refresh token
func validateRefreshToken(tokenString string, key string) (*refreshTokenCustomClaims, error) {
	claims := &refreshTokenCustomClaims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(key), nil
	})

	// For now we'll just return the error and handle logging in service level
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, fmt.Errorf("refresh token is invalid")
	}

	claims, ok := token.Claims.(*refreshTokenCustomClaims)

	if !ok {
		return nil, fmt.Errorf("refresh token valid but couldn't parse claims")
	}

	return claims, nil
}

// validateAccessToken returns the token's claims if the token is valid
func validateAccessToken(tokenString string, key *rsa.PublicKey) (*accessTokenCustomClaims, error) {
	claims := &accessTokenCustomClaims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})

	// For now we'll just return the error and handle logging in service level
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, fmt.Errorf("ID token is invalid")
	}

	claims, ok := token.Claims.(*accessTokenCustomClaims)

	if !ok {
		return nil, fmt.Errorf("ID token valid but couldn't parse claims")
	}

	return claims, nil
}
